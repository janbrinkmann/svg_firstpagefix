<?php

namespace SVG\FirstPageAfterLogin\Model;

use Magento\Framework\Event\ObserverInterface;

class Observer implements ObserverInterface
{
    /**
     * @var \Magento\Backend\Model\Auth\StorageInterface
     */
    protected $_authStorage;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $_backendSession;

    /**
     * Observer constructor.
     * @param \Magento\Backend\Model\Auth\StorageInterface $authStorage
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        \Magento\Backend\Model\Auth\StorageInterface $authStorage,
        \Magento\Backend\Model\Session $backendSession
    )
    {
        $this->_backendSession = $backendSession;
        $this->_authStorage = $authStorage;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_alreadyVisited()) {
            return;
        }

        if ($this->_authStorage->isLoggedIn()) {
            $this->_saveLoginFlag();
        }
    }

    /**
     * @return mixed
     */
    protected function _alreadyVisited()
    {
        return $this->_backendSession->getAlreadyVisited();
    }

    /**
     *
     */
    protected function _saveLoginFlag()
    {
        $this->_authStorage->setIsFirstPageAfterLogin(false);

        // remember
        $this->_backendSession->setAlreadyVisited(true);
    }
}